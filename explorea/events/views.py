from django.shortcuts import render, HttpResponse, get_object_or_404

from .models import Event,EventRun


def event_listing(request):
	
	events = Event.objects.all()
	
	return render(request, 'events/event_listing.html', {'events': events})

def event_detail(request,pk):
	
	details=get_object_or_404(Event, pk=pk)
	runs = EventRun.objects.filter(event=pk)
	
	return render(request, 'events/event_detail.html', {'detail': details,'runs': runs})
