from . import views
from django.urls import path

urlpatterns = [
    path('', views.event_listing, name='event_listing'),
    path('detail/<int:pk>/', views.event_detail, name='detail'),
]